#!/bin/sh
#
# Install script for chaperone inside a Docker container.
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES=""

# Packages required to serve the website and run the services.
PACKAGES="prometheus-pushgateway ca-certificates"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

apt-get -q update

# Install required packages
install_packages ${BUILD_PACKAGES} ${PACKAGES} \
    || die "could not install packages"

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
