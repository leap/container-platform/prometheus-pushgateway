FROM debian:stable-slim

RUN apt-get update \
 && apt-get install -y sudo
RUN adduser --disabled-password --gecos '' docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER docker

COPY build.sh /tmp/build.sh 
RUN sudo /tmp/build.sh && sudo rm /tmp/build.sh

ENTRYPOINT ["/usr/bin/prometheus-pushgateway"]

