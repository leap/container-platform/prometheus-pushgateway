# Prometheus Pushgateway

This repository contains a Docker build script to run prometheus-pushgateway in a container. It's used to provide OONI network testing results fetched by the [OONI-Exporter](https://0xacab.org/leap/ooni-exporter) to Prometheus.